const { GraphQLServer } = require('graphql-yoga')
const {PubSub} = require ('graphql-yoga')
const query = require('./resolvers/queries.js')
const users= require('./users/users.js')
const mutation = require('./resolvers/mutations.js')
const subscription= require('./resolvers/subscriptions.js')

    const resolvers= [mutation.mutation, query.query,subscription.subscription]
    const pubsub= new PubSub()
      const context= (req) =>({
        req: req.request,
        pubsub
      })

const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers,
  context,



})
server.start(() => console.log(`Server is running on http://localhost:4000`))

  
