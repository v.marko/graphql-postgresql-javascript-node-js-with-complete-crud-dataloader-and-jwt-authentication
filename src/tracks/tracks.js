const d= require('../db/db.js')
const Dataloader = require('dataloader')
const R = require('ramda')

exports.getAllTracks = async function(arg) {
  console.log("getAllTracks");
  var result = await d.db.query(`SELECT * FROM tracks WHERE album_id=${arg}`)
    return result.rows

}


async function getAllTracksLoader(albumIds) {
    console.log("getAllTracksLoader");
    const sql =`SELECT * FROM tracks WHERE album_id = ANY($1) ORDER BY id;`;
    const params= [albumIds]
      const result = await d.db.query(sql,params);
        return result.rows
}


async  function batchTracks(albumIds) {
    const albums = await getAllTracksLoader(albumIds)
    const groupedById = R.groupBy( album => album.album_id, albums );

    return R.map(albumId => groupedById[albumId] , albumIds)


  }
  exports.tracksLoader= new Dataloader(batchTracks)
