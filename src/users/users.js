  require('dotenv').config()
const d= require('../db/db.js')
const bcrypt = require('bcryptjs')
const crypto = require('crypto')
const jwt = require('jsonwebtoken')
const mutation=require('../resolvers/mutations.js')


  exports.createUser= async function(username,password) {
            const err={ emptyUsername: 'Please provide username',
                        emptypassword: 'Please provide password',
                        success: 'User added to database',
                        existingUsername: 'Username already exist, please change your username',
                        accessDenied: 'You dont have access',
                        accessDenied2: 'You dont have access 2',
                        }


              const hashedPass= await bcrypt.hash(password,10)
              const sql=`INSERT INTO users VALUES(default,'${username}','${hashedPass}');`
              if( username== "" || username== null){
                return err.emptyUsername
              }
                else if ( password=="" || password==null) {
                    return err.emptypassword
                }
                  else {
                      try {
                        await d.db.query(sql)
                          return err.success
                      } catch (e) {
                          return err.existingUsername
                      }

                  }

                  }

                 function generateAccessToken(user) {
                   return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET,{expiresIn: '240s'})
                 }



    exports.loginUser= async function (username,password) {
                  const err={emptyUsername: 'Please provide username',
                              emptypassword: 'Please provide password',
                              usernameNotExist: 'WRONG USERNAME',
                              logedIn: 'USER IS LOGED IN',
                              wrongPass:'Password is Wrong'

                  }


                    const sql=`SELECT * FROM users WHERE username='${username}'`
                    if( username== "" || username== null){

                      return err.emptyUsername
                    }
                      else if ( password=="" || password==null) {

                          return err.emptypassword
                      }
                        else {

                       const result=await d.db.query(sql)

                          if (result.rowCount==0) {

                              return err.usernameNotExist
                          }
                              else {
                                    if ( await bcrypt.compare(password,result.rows[0].password)) {
                                      const id= result.rows[0].id
                                      const admin = result.rows[0].admin
                                      const user={ id: id, isAdmin: admin}
                                      const accessToken= generateAccessToken(user)
                                      const refreshToken = jwt.sign(user, process.env.REFRESH_TOKEN_SECRET,{expiresIn:'60s'})
                                                // EXPORTED FUNCTION FROM LOGIN RESOLVER THAT IS USED FOR SUBSCRIPTION OF USERS
                                            mutation.cxt()

                                  return  "ACCESS TOKEN: " + accessToken + "     " +"REFRESH TOKEN: " + refreshToken
                                    }
                                    else {

                                      return err.wrongPass
                                    }
                              }



    }
  }


    exports.authToken= function(authHeader) {
      const e2={invalidToken: 'Invalid token',
        expiredToken: 'Your token has expired' }

          const token = authHeader.split(" ")[1]
          if (token==null){ return e2.invalidToken  }
          else{

            try {
              var user= jwt.verify(token, process.env.ACCESS_TOKEN_SECRET)
                      return user.isAdmin
            } catch (e) {
                  return e2.expiredToken
            }
 }
  }

      exports.generateNewToken = function(token){
        const refreshToken = token
          if (refreshToken==null) return "Your token has expired"

        const accessToken = jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err,user)=>{

            if (err) return "Your token has expired"
            const thisUser={id: user.id, isAdmin: user.isAdmin}
                return generateAccessToken(thisUser)

          })

                  return accessToken

      }




        //  USED TO CREATE ACCESS AND REFRESH TOKEN SECRETS IN DOT.ENV
         //const pok= crypto.randomBytes(64).toString('hex')
            // console.log(pok);
