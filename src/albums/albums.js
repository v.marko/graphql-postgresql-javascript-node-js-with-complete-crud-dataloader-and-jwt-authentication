const d= require('../db/db.js')
const Dataloader = require('dataloader')
const R = require ('ramda')

exports.getAllAlbums= async function (arg) {
    console.log("getAllAlbums");
      const result = await d.db.query(`SELECT albums.id,albums.name,
        albums.length,to_char(albums.date_released,'DD Mon YYYY')
       AS date_released FROM albums WHERE id=ANY
        (SELECT albums_id FROM artists_albums WHERE artists_id=${arg})`)
        return result.rows
    }


    async function getAllAlbumsLoader() {
      console.log("getAllAlbumsLoader");
      const sql=`SELECT albums.id,albums.name,albums.length,to_char(albums.date_released,'DD Mon YYYY')
       AS date_released,artists_id FROM albums,artists_albums WHERE albums.id=artists_albums.albums_id`
      const result = await d.db.query(sql)
      return result.rows
    }

    async  function batchAlbums(artistsIds) {
        const artists = await getAllAlbumsLoader()
        const groupedById = R.groupBy( artist => artist.artists_id, artists );
        return R.map(artistId => groupedById[artistId] , artistsIds)
      }


      exports.albumsLoader= new Dataloader(batchAlbums)

            const err={
                      emtpyName: "Please insert album name",
                      emptyLength: "Please insert album length in this format mm:ss",
                      emptyDate: "Please insert date in this format YYYY:MM:DD",
                      emptyArtistId: "Please insert id of the artist",
                      albumCreated: "Album is created successfully",
                      error: "Something wrong happened, please try again",
                      albumDeleted: "Album is successfully deleted",
                      errorAlbumId: "There is no album with that id in database",
                      albumUpdated: "Album is successfully updated"
            }
      exports.createAlbum = async function (idArtist,name,date_released,length) {
          const sql= `WITH dual AS(INSERT INTO albums VALUES(default,'${name}','${date_released}','${length}') RETURNING id)
           INSERT INTO artists_albums VALUES((SELECT id FROM dual),${idArtist});`
           if( name== "" || name== null || typeof name!="string"){
             return err.emptyName
           }
             else if ( length=="" || length==null || typeof length!="string"
              || length< "00:00" || length > "99:59" || length.length!=5
            || length[2]!=":" || length[3]>5) {
                 return err.emptyLength
             }
              else if (date_released=="" || date_released== null || typeof date_released!="string") {
                        return err.emptyDate
              }
              else if (idArtist=="" || idArtist== null || typeof idArtist!="number") {
                        return err.emptyArtistId
              }
               else {
                   try {
                     await d.db.query(sql)
                           return err.albumCreated

         }
                 catch{
                           return err.error
                 }
               }


        }
      exports.deleteAlbum = async function(id) {
        const sql= `DELETE FROM artists_albums WHERE albums_id=${id};
                    DELETE FROM albums WHERE id=${id};`
                    if( id== "" || id== null || typeof id!="number"){
                      return err.emptyArtistId
                    }
                      else {
                    var result=  await d.db.query(sql)

                        if (result[1].rowCount==0) {
                              return err.errorAlbumId
                         }
                         else {
                            return err.albumDeleted
                         }

                      }

      }
      exports.updateAlbum = async function (id,name,date_released,length) {
        const sql=`UPDATE albums SET name='${name}',  date_released='${date_released}',
                length='${length}' WHERE id=${id};`

                if( name== "" || name== null || typeof name!="string"){
                  return err.emptyName
                }
                  else if ( length=="" || length==null || typeof length!="string"
                   || length< "00:00" || length > "99:59" || length.length!=5
                 || length[2]!=":" || length[3]>5) {
                      return err.emptyLength
                  }
                   else if (date_released=="" || date_released== null || typeof date_released!="string") {
                             return err.emptyDate
                   }
                   else if (id=="" || id== null || typeof id!="number") {
                             return err.errorAlbumId
                   }
                    else {
                      try {
                            const result=  await d.db.query(sql)
                            if (result.rowCount==0) {
                                  return err.errorAlbumId
                            }
                              else {
                                return err.albumUpdated
                              }

                      } catch (e) {
                              return err.error
                      }
                    }


      }
