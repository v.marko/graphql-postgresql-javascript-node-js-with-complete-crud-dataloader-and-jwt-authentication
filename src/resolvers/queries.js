const { GraphQLServer } = require('graphql-yoga')
const artists= require('../artists/artists.js')
const albums = require('../albums/albums.js')
const tracks = require('../tracks/tracks.js')
const genres = require('../genres/genres.js')
const users = require('../users/users.js')



exports.query={
  Query: {
      artists: () => artists.getAllArtists(),
      albumsByArtistId:(root,args,context) => albums.albumsLoader.load(args.ArtistId)

  },

    Artist:{
    /* NO D LOADER */album: (ArtistObj,args) => albums.getAllAlbums(ArtistObj.id)
    //album:(ArtistObj,args) => albums.albumsLoader.load(ArtistObj.id)
  },
   Album:{
        /*NO D LOADER */ tracks:(AlbumObj,args) => tracks.getAllTracks(AlbumObj.id),
    // tracks: (AlbumObj,args) => tracks.tracksLoader.load(AlbumObj.id),
     /* NO D LOADER*/ genres: (AlbumObj,args) => genres.getAllGenres(AlbumObj.id)
     //genres: (AlbumObj,args) => genres.genresLoader.load(AlbumObj.id)
   }
}
