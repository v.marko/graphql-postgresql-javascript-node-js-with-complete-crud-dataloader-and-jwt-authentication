const { GraphQLServer } = require('graphql-yoga')
const artists= require('../artists/artists.js')
const albums = require('../albums/albums.js')
const tracks = require('../tracks/tracks.js')
const genres = require('../genres/genres.js')
const users = require('../users/users.js')




exports.mutation={
  Mutation: {
      createArtist:(root,args,context) => {

              const header= context.req.headers.authorization
              const isAdmin= users.authToken(header)
                 if (isAdmin== true) {
                    return artists.createArtist(args.name,args.biography)

                }
                  else if(isAdmin== false){
                    return "You aren't allowed to do that, only admin can change data"
                  }
                  else {
                    return isAdmin
                  }

},
    deleteArtist:(root,args,context) => {
      const header= context.req.headers.authorization
      const isAdmin= users.authToken(header)
      if (isAdmin== true) {
         return  artists.deleteArtist(args.id)

     }
            else if(isAdmin== false){
              return "You aren't allowed to do that, only admin can change data"
            }
            else {
              return isAdmin
            }


},
    updateArtist:(root,args,context) =>{
      const header= context.req.headers.authorization
      const isAdmin= users.authToken(header)
      if (isAdmin== true) {
         return artists.updateArtist(args.id,args.name,args.biography)

     }
     else if(isAdmin== false){
       return "You aren't allowed to do that, only admin can change data"
     }
     else {
       return isAdmin
     }




    },
    createAlbum:(root,args,context) =>{
          const header= context.req.headers.authorization
          const isAdmin= users.authToken(header)
             if (isAdmin== true) {
                return  albums.createAlbum(args.idArtist,args.name,args.date_released,args.length)

            }
              else if(isAdmin== false){
                return "You aren't allowed to do that, only admin can change data"
              }
              else {
                return isAdmin
              }

    },
    deleteAlbum:(root,args,context) =>{
      const header= context.req.headers.authorization
      const isAdmin= users.authToken(header)
         if (isAdmin== true) {
            return  albums.deleteAlbum(args.id)

        }
          else if(isAdmin== false){
            return "You aren't allowed to do that, only admin can change data"
          }
          else {
            return isAdmin
          }
},
    updateAlbum:(root,args,context) =>{
      const header= context.req.headers.authorization
      const isAdmin= users.authToken(header)
         if (isAdmin== true) {
            return  albums.updateAlbum(args.id,args.name,args.date_released,args.length)

        }
          else if(isAdmin== false){
            return "You aren't allowed to do that, only admin can change data"
          }
          else {
            return isAdmin
          }
    },
    createUser:(root,args) => users.createUser(args.username,args.password),
    loginUser:(root,args,context) =>{



          exports.cxt = function(){context.pubsub.publish('users', {
                     users:{
                           message: `User ${args.username} just signed in`,

                        }
                      }) }
                        return   users.loginUser(args.username, args.password)



},

    generateNewToken:(root,args) => users.generateNewToken(args.token)

},

}
