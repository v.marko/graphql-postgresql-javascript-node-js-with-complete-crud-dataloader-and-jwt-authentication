const { GraphQLServer } = require('graphql-yoga')
const artists= require('../artists/artists.js')
const albums = require('../albums/albums.js')
const tracks = require('../tracks/tracks.js')
const genres = require('../genres/genres.js')
const users = require('../users/users.js')

exports.subscription={
  Subscription: {
    users:{
      subscribe(parent,args,context,info){
        return context.pubsub.asyncIterator('users')
      }
    }

  }
}
