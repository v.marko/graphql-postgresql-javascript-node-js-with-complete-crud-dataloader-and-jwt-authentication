
const d= require('../db/db.js')

exports.getAllArtists = async function () {
  console.log("getAllArtists");
  var result = await d.db.query('SELECT * FROM artists')
  return result.rows
}

const err={
        emptyName: "Please insert artist name",
        emptyBiography: "Please insert artist biography",
        artistCreated: "Artist is created successfully",
        error: "Something wrong happened, please try again",
        emptyId: "Please insert id of the artist",
        artistDeleted: " Artist is deleted successfully",
        artistUpdated: "Artist is updated successfully"
}
exports.createArtist = async function (name,biography) {
    const sql=`INSERT INTO artists VALUES (default,'${name}','${biography}')`;
    if( name== "" || name== null || typeof name!="string"){
      return err.emptyName
    }
      else if ( biography=="" || biography==null || typeof biography!="string") {
          return err.emptyBiography
      }
        else {
            try {
              await d.db.query(sql)
                    return err.artistCreated

  }
          catch{
                    return err.error
          }
        }
      }

  exports.deleteArtist = async function (id) {
    const sql=` DELETE FROM artists WHERE id =${id}`
            if(id == 0 || id==null || typeof id != "number"){
                  return err.emptyId
            }
              else {
                try {
                      await d.db.query(sql)
                      return err.artistDeleted
                } catch (e) {
                        return err.error

              }
  }
}

  exports.updateArtist = async function (id,name,biography) {
      const sql =`UPDATE artists SET name='${name}', biography='${biography}' WHERE id=${id}`
      if(id == 0 || id==null || typeof id != "number"){
            return err.emptyId
          }
            else if( name== "" || name== null || typeof name!="string"){
              return err.emptyName
            }
            else if ( biography=="" || biography==null || typeof biography!="string") {
                return err.emptyBiography
            }
            else {
                try {
                  await d.db.query(sql)
                        return err.artistUpdated

      }
              catch{
                        return err.error
              }
            }

  }
