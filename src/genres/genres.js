const d= require('../db/db.js')
const Dataloader = require('dataloader')
const R = require ('ramda')

exports.getAllGenres= async function(arg) {
  console.log("CETVRTA FUNKCIJA");
  var result = await d.db.query(`SELECT * FROM genres WHERE id
     IN((SELECT genres_id FROM albums_genres WHERE albums_id=${arg}))` )
    return result.rows
}


async function getAllGenresLoader() {
  console.log("getAllGenresLoader");
  const sql= `SELECT genres.*,albums_id FROM genres,albums_genres  WHERE genres.id=albums_genres.genres_id`
  const result = await d.db.query(sql)
  return result.rows
}

async  function batchGenres(genresIds) {
    const genres = await getAllGenresLoader()
    const groupedById = R.groupBy( genre => genre.albums_id, genres );
    return R.map(genreId => groupedById[genreId] , genresIds)
  }


  exports.genresLoader= new Dataloader(batchGenres)
