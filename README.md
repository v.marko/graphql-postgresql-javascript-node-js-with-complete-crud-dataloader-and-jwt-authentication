# GraphQL PostgreSQL  javascript node js with complete CRUD dataloader and jwt authentication

GraphQL back end project created on graphql-yoga server, with PostgreSQL database using javascript and node js.
Examples of queries, mutations, subscription, N+1 problem and resolving it with Dataloader, authentication with jwt ,etc...  


I had no any experience with GraphQL, PostgreSQL or js before this project, so this is my first project.

###### 
**THINGS TO SET UP PROJECT**

 - You need to make your own PostgreSQL database with the same model that I provided, and provide database info in file src/db/db.js
 (I used SERIAL (AUTO INCREMENT) in all tables id, also its good to mention that in table USER column username should be uniqe )

 INSTALL USED LIBRARIES AND MODULES USED IN THIS PROJECT: 
  -  initialize npm in project directory:  ## npm init -y
  -  add graphql-yoga server:  ## npm add graphql-yoga
  -  add pg, that is used for database: ## npm i pg
  -  add Dataloader for solving N+1 problem: ## npm install --save dataloader
  -  add Ramda library to use GROUPBY and MAP functions in Dataloader: ## npm install ramda
  -  add Bcrypt for hashing passwords: ## npm install --save bcryptjs
  -  add jwt for tokens: ## npm install jsonwebtoken
  -  add dotenv to store salt for token and refresh token: ## npm install dotenv
  -  I created salt for tokens with crypto library, and you dont need to use it beacuse salts are already in dotenv file, but anyway here it is: ## npm i crypto
  -  Finaly you can install node-fetch library for file page that is simple html that shows primitive way to call your api from front end: ## npm install node-fetch --save


  **TO START PROJECT :  START CMD IN YOUR PROJECT FOLDER AND TYPE: node src/index.js   - THIS WILL START GRAPHQL-YOGA SERVER ON YOUR LOCALHOST, AND YOU CAN USE IT IN BROWSER**
  
 ** GRAPHQL-YOGA IS USED FOR SERVER ,AND YOU CAN TEST ALL APIs  IN YOUR BROWSER**
######

 Database is a sipmle songs database that contains artists, albums, genres and tracks, and joining tables that breaks many to many relationships.
 Also there is table USER that contain username, password and boolean type column isAdmin. You dont need to log in to see content from database.
 In order to change database you need to be loged in, and that user needs to be admin. If he is not admin he cant change database either.
 
######

 TOKEN HAS 120s before expiration, REFRESH TOKEN HAS 240s before expiration, REFRESH TOKEN is used to create new ACCESS TOKEN with GENERATE ACCESS TOKEN API
 You can change time expiration if its short for you, but its made like this to easily show authentication and authorization. 
 
######
 
  GRAPHQL-YOGA IS USED FOR SERVER ,AND YOU CAN TEST ALL FUNCTIONS IN YOUR BROWSER
 Bcrypt is used to hash passwords, and if all authentication is passed then user will get token.
 
######

 Resolvers are splited in queries, mutations and subscriptions.
 
 In queries you have only public APIs and you can change them to show N+1 problem, or change resolvers to use Dataloder to solve it:
 UNCOMENT CODE THAT IS COMENTED, AND put THE OTHER ONE IN COMENTS... IN CONSOLE YOU SHOULD SEE HOW MANY TIMES FUNCTIONS ARE CALLED, THAT WILL SHOW N+1 PROBLEM

######

    Mutation contains protected APIs, and USER needs to be loged in and has admin status to use them.
    
######
 
    SUBSCRIPTION IS USED TO SHOW LOG IN OF USERS IN REAL TIME , YOU COULD CHECK THAT ALSO.
    
######
        ######     HAVE FUN AND FEEL FREE TO MESSAGE ME IF YOU HAVE ANY TROUBLES, OR FOR ANY REASON     ######
                        MESSAGE ME : vukadinovicmarko1@gmail.com
 